# Photon Transit Model for LZ Detector



## Simulation

Photons transit time is simulated in BACCARAT by putting a point photon bump source on a specific radius and different heights.

Since there is no radius dependence on photon transit time, any radius can be used. For generating the photon transit model in NEST, we use simulated data with r=9.428 cm.

Simulation is done by Anna David and can be found in '/global/cfs/projectdirs/lz/users/adavid/SimsTuning'.

In this simulation, 10 million photons spread over 15 height slices are generated. It may not be unformly distributed over these slices. Each height slice is 100 mm, for which the distribution of photon transit time can be obtained. For this histogram, each time bin is 1 ns.

There are a small amount of photons which haven't experienced any scatters or reflection and are directly detected. With the dimension of the detector and the location of photons, we can calculate the max time for photons to be directly detected. For example, if the photon is located at the top of the detector, the longest path will be photon detected by bottom edge PMT. So, the max time is expected to be smaller for photon located at the center of the detector.

So, we separate the photon transit time distribution into two parts: distribution with t < max time and distribution with t > max time.

We consider distribution up to time for which 99% of total counts is achieved. For further times, the statistic is too low for each time bin.

We use double exponential function to model the photon transit times for which photons are NOT directly detected.

## Analysis Procedure
Calculate the ratio of photons detected by top PMT array to photons detected by bottom PMT array. Check counts_top+counts_bot=counts_total
Calculate the max time when photon are directly detected. T_direct = longest path/speed of light in LXe.
Calculate and only include distribution up to time when 99% area is achieved.
Obtain the times & counts for which photons are directly detected
Exclude times & counts for which photons are directly detected
Calculate sigma of each bin=sqrt(counts of each bin), and apply normalization
Fit double exponential function with sigma, bounds can be added.
Obtain the fitting results and errors of each parameter
Calculate the relative difference as abs(fit-count)/count for each bin
Calculate chisq of each fit with sigma calculated above. This assumes error of each point accords Poisson distribution.
